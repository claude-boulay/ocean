<?php

use App\Models\Auteur;
use App\Models\Serie;

?>
<center>
    <form method="post" action="creation_livre">
        <br><br>
        <label for="titre">Titre</label>
        <input type="text" name="titre" id="titre" required><br><br>

        <label for="nbr">Nombre d'exemplaire</label>
        <input type="text" name="nbr" id="nbr" required><br><br>

        <label for="serie">nom série</label>
        <select name="serie" id="serie">
            <?php 
            $series=Serie::all();
            foreach ($series as $serie) :?>
                <option value="<?= $serie->id_serie?>"><?= $serie->nom_série?></option>
            <?php endforeach?>
        </select>

        <br><br><label for="auteur">Auteur</label>
        <div id="duplicata">
        <select name="auteur[]" id="auteur">
            <?php 
                $auteurs=Auteur::all();
                foreach ($auteurs as $auteur) :?>
                <option value="<?= $auteur->id_auteur?>"><?= $auteur->nom_auteur?></option>
            <?php endforeach?>
        </select>
        </div>

        <button id="ajoutAuteur" type="button" >+</button>
        <button id="suppresslastAuteur" type="button">-</button>
        <br><br><label for="categorie">Catégorie</label>

        <select name="categorie" id="categorie">
            <option value="1">Romance</option>
            <option value="2">Poétiques</option>
            <option value="3">Épopée</option>
            <option value="4">Fable</option>
            <option value="5">Nouvelle</option>
            <option value="6">Roman Policier</option>
            <option value="7">Science-fiction</option>
            <option value="8">Fantasy</option>
            <option value="9">Fantastique</option>
            <option value="10">Conte</option>
            <option value="12">Témoignage</option>
        </select>
        
        <br><br>
        <input type="submit" name="auteurValid" value="Valider">
       
        
    
    </form>
</center>
<script>
    document.addEventListener('DOMContentLoaded', function(){
        button=document.getElementById("ajoutAuteur");
        suppres=document.getElementById("suppresslastAuteur");
        count=1;
    button.addEventListener("click", function(){
        auteur=document.getElementById("auteur");
        auteur.insertAdjacentHTML("afterend",'<select id='+count+' name="auteur[]">'+auteur.innerHTML+'</select>');
        count+=1;
    
    })
    suppres.addEventListener("click",function(){
        auteurremove=document.getElementById(count-1);
        auteurremove.remove();
        count-=1;
    })
    
    })
    
</script>