<center>
    <form method="post" action="creation_utilisateur">
        <br><br>
        <label for="nom_user">Nom d'utilisateur</label>
        <input type="text" name="nom_user" id="nom_user" required><br><br>

        <label for="prenom_user">Prénom d'utilisateur</label>
        <input type="text" name="prenom_user" id="prenom_user" required><br><br>

        <label for="mail">Email</label>
        <input type="email" name="mail" pattern=".+@.+.com" id="mail" required><br><br>

        <label for="mdp1">Mot de passe</label>
        <input type="password" pattern="^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,}).{8,}$" name="mdp1" id="mdp1" required><br><br>

        <label for="mdp2">Vérification mot de passe</label>
        <input type="password" pattern="^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,}).{8,}$"name="mdp2" id="mdp2" required><br><br>

        <label for="admin">Admin :</label>
        <input type="checkbox" name="admin" value="true" id="admin"><br><br>
      

        
        
        <input type="submit" name="auteur" value="Valider">
    </form>
</center>