<center>
    <form method="post" action="creation_auteur">
        <br><br>
        <label for="nom_auteur">Nom de l'auteur</label>
        <input type="text" name="nom_auteur" id="nom_auteur" required><br><br>

        <label for="prenom_auteur">Prenom de l'auteur</label>
        <input type="text" name="prenom_auteur" id="prenom_auteur" required><br><br>

        <label for="naissance">Date de naissance de l'auteur</label>
        <input type="date" name="naissance" id="naissance"><br><br>

        <label for="deces">Date de décés de l'auteur</label>
        <input type="date" name="deces" id="deces"><br><br>

        <input type="submit" name="auteur" value="Valider">
    </form>
</center>