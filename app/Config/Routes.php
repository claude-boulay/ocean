<?php

use App\Models\Utilisateur;
use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
$routes->get('/', 'Home::index');
$routes->get('Accueil', 'Home::index');
$routes->get('inscription', 'Utilisateurs::creationUtilisateur',['filter'=>'connecter']);
$routes->get("création_livre","Livres::creationLivre",['filter'=>'connecter']);
$routes->get("création_auteur","Auteurs::creationAuteur",['filter'=>'connecter']);
$routes->get("création_serie","Series::creationSerie",['filter'=>'connecter']);
$routes->get("création_categorie","Categories::creationCategorie",['filter'=>'connecter']);
$routes->post("creation_auteur","Auteurs::validationCreationAuteur",['filter'=>'connecter']);
$routes->post("creation_categorie","Categories::validationCreationCategorie",['filter'=>'connecter']);

$routes->post("creation_serie","Series::validationCreationSerie",['filter'=>'connecter']);
$routes->post("creation_utilisateur","Utilisateurs::validCreationUser",['filter'=>'connecter']);
$routes->get("connexion","Utilisateurs::connexion");
$routes->post("connexion","Utilisateurs::validConnexion");
$routes->post("creation_livre","Livres::creationLivreValidation",['filter'=>'connecter']);



//webservices
$routes->get("webservice/getCategories","WebService::getCategories");
$routes->get("webservice/getLivreByCategorie/(:num)","WebService::getLivreByCategorie/$1");
$routes->get("webservice/getSeries","WebService::getSeries");
$routes->get("webservice/getLivres","WebService::getLivres");
$routes->get("webservice/getBookBySeries/(:num)","WebService::getBookBySeries/$1");
$routes->get("webservice/getSerie/(:num)","WebService::getSerie/$1");