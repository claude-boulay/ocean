<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class LogUser extends Model{
    public $timestamps=false;

    protected $table="log_user";

    protected $primaryKey="id_modif";
}