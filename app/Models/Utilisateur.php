<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Utilisateur extends Model{
    public $timestamps=false;

    protected $table="Utilisateur";

    protected $primaryKey="id_utilisateur";
}