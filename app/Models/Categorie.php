<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Categorie extends Model{
    public $timestamps=false;

    protected $table="Categorie";

    protected $primaryKey="id_categorie";

    public function Livres(){
        return $this->belongsToMany("App\Models\Livre","Avoir","id_categorie","id_livre");
    }
}