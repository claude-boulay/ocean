<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\Models\Auteur;

class Livre extends Model{
    public $timestamps=false;

    protected $table="Livre";

    protected $primaryKey="id_livre";

    public function ecrit(){
        return $this->belongsToMany('\App\Models\Auteur',"Ecrit","livre_id","auteur_id");
    }

    public function ajoutLigneEcrit($idLivre,$idAuteur){
        $ecrit=Livre::find($idLivre);
        $ecrit->ecrit()->attach($idAuteur);
    }

    public function Avoir(){
        return $this->belongsToMany('\App\Models\Categorie',"Avoir","id_livre","id_categorie");
    }

    public function AvoirCategorie($idLivre,$idCategorie){
        $avoir=Livre::find($idLivre);
        $avoir->Avoir()->attach($idCategorie);
    }
}