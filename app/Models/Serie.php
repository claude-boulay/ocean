<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Serie extends Model{
    public $timestamps=false;

    protected $table="Serie";

    protected $primaryKey="id_serie";

    public function Livre(){
        return $this->hasMany("App\Models\Livre","id_serie");
    }
}