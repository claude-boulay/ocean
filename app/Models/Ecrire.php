<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Ecrire extends Model{
    public $timestamps=false;

    protected $table="Ecrit";

    protected $primaryKey="ecrit";
}