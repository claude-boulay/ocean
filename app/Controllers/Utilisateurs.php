<?php
namespace App\Controllers;


use App\Models\Utilisateur;
use Exception;

class Utilisateurs extends BaseController
{ 
    public function creationUtilisateur(){
        echo view("templates/header");
        echo view("creation_utilisateur");
    }

    public function validCreationUser(){
        if($this->request->getMethod() === 'post'){
            if($this->validate([
                'nom_user' => 'required|min_length[4]|max_length[50]',
                'prenom_user' => 'required|min_length[4]|max_length[50]',
                'mail'=> "required|min_length[6]|max_length[50]|valid_email|is_unique[Utilisateur.mail_user]",
                
            ])){
                $nom_user=$this->request->getPost("nom_user");
                $prenom_user=$this->request->getPost("prenom_user");
                $mail=$this->request->getPost("mail");
                $password=$_POST["mdp1"];
                $password_verif=$_POST["mdp2"];
                //check si l'utilisateur serat un admin
                if($this->request->getPost("admin")){
                    $admin=true;
                }else{
                    $admin=false;
                }
                //check si les mot de passe correspondent
                if($password==$password_verif){
                   
                    $passwordF=password_hash($password,PASSWORD_BCRYPT);
                   
                    $user=new Utilisateur();
                    $user->nom_utilisateur=$nom_user;
                    $user->prenom_utilisateur=$prenom_user;
                    $user->mail_user=$mail;
                    $user->mdp_user=$passwordF;
                    $user->admin=$admin;
                //sauvegarder dans la base de données
                $user->save();
                echo view('templates/header');
                return view('index');
                }else{
                    echo view("templates/header");

                    return view("Echec");
                }
                //attribuer les valeur au attribut correspondant de la classe USER
                
            
            }else{
                echo view("templates/header");
                return view("creation_utilisateur");
                
            }


        }
    }

    public function connexion(){
        echo view("templates/header");
        echo view("connexion");
    }

    public function validConnexion(){
        if ($this->request->getMethod()==="post") {
            $login=$this->request->getPost("login");
            $password=$_POST["password"];
            $session=session();
            try{
                $user=Utilisateur::where("mail_user",$login)->first();
            }catch(Exception $e){
                $data["error"]="email ou mot de passe incorrect";
                echo view("templates/header");
                return view("connexion",$data);
            }
           
            $pass=$user["mdp_user"];

            $authentification=password_verify($password,$pass);
            
            if($authentification){
                $ses_data=['id'=>$user["id_utilisateur"],
                "nom_user"=>$user['nom_utilisateur'],
                "prenom_user"=>$user["prenom_utilisateur"],
                "mail_user"=>$user["mail_user"],
                "admin"=>$user["admin"],
                "connected"=>true];
                
                    $session->set($ses_data);
                echo view("templates/header");
                return view('index');
            }else{
               $session->setFlashdata('msg', 'Password is incorrect.');
               echo view("templates/header");
                return view('connexion');
            } 
        }else{
            echo view("templates/header");
            return view("Echec");
        }
    }
}