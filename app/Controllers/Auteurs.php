<?php
namespace App\Controllers;

use App\Models\Auteur;

class Auteurs extends BaseController
{ 
    public function creationAuteur(){

        echo view("templates/header");
        echo view("creation_auteur");
        echo view("templates/footer");
    }

    public function validationCreationAuteur(){
        

        if ($this->request->getMethod() === 'post') {
            //on traite les données postées
            if ($this->validate([
                'nom_auteur' => 'required|min_length[3]|max_length[255]',
                'prenom_auteur' => 'required|min_length[3]|max_length[255]',
            ])) {
                //la validation a réussi
                $Auteur = new Auteur();

                $Auteur->nom_auteur= $this->request->getPost('nom_auteur');
                $Auteur->prenom_auteur=$this->request->getPost('prenom_auteur');
                if($this->request->getPost("naissance")){
                    $Auteur->naissance=$this->request->getPost("naissance");
                }
                if($this->request->getPost("deces")){
                    $Auteur->décés=$this->request->getPost("deces");
                }
                $Auteur->save();
                echo view("templates/header");
                echo view("Validation");
                echo view("templates/footer");
            } else {
                //la validation a échoué
                echo view('citations/templates/header', ['title' => 'Saisir
            une citation']);
                //on transmet les erreurs de validation
                $data['validation'] = $this->validator;
                echo view("templates/header");
                echo view("Echec",$data);
                echo view("templates/footer");
            }
        } else {
            //Nous affichons le form de creation
            //d'une nouvelle citation
            echo view("templates/header");
            echo view("creation_auteur");
            echo view("templates/footer");

        }

        //$this->request->;
    }
}
