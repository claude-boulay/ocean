<?php
namespace App\Controllers;

use App\Models\Categorie;

class Categories extends BaseController
{ 
    public function creationCategorie(){

        echo view("templates/header");
        echo view("creation_categorie");
    }

    public function validationCreationCategorie(){
        if ($this->request->getMethod() === 'post') {
            //on traite les données postées
            if ($this->validate([
                'nom_categorie' => 'required|min_length[3]|max_length[255]'
               
            ])) {
                //la validation a réussi
                $Categorie = new Categorie();

                $Categorie->nom_categorie= $this->request->getPost('nom_categorie');
                
                $Categorie->save();
                echo view("templates/header");
                echo view("Validation");
            } else {
                //la validation a échoué
                
                //on transmet les erreurs de validation
                $data['validation'] = $this->validator;
                echo view("templates/header");
                echo view("Echec",$data);
            }
        } else {
            //Nous affichons le form de creation
            //d'une nouvelle citation
            echo view("templates/header");
            echo view("creation_categorie");

        }
    }
}
