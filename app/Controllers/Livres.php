<?php
namespace App\Controllers;

use App\Models\Categorie;
use App\Models\Auteur;
use App\Models\Livre;
use Exception;

class Livres extends BaseController
{ 
    public function creationLivre(){

        echo view("templates/header");
        echo view("creation_livre");
    }

    public function creationLivreValidation(){
    
        $titre=$this->request->getPost("titre");
        $nbr=$this->request->getPost("nbr");
        $serie=$this->request->getPost("serie");
        $auteurs=$this->request->getPost("auteur");
        $categorie=$this->request->getPost("categorie");
        try{
        $livre=new Livre();
        $livre->titre=$titre;
        $livre->nbr_exemplaire=$nbr;
        $livre->id_serie=$serie;
        $livre->save();

        $Livre=Livre::where("titre",$titre)->first();

        foreach($auteurs as $auteur){
           $Livre->ajoutLigneEcrit($Livre->id_livre,$auteur);
        }

       $Livre->AvoirCategorie($Livre->id_livre,$categorie);
        

        echo view("templates/header.php");
        echo view("Validation.php");
        return view("templates/footer.php");
    }catch(Exception $e){
        echo view("templates/header.php");
        echo "erreur lors de la création du livre";
        return view("creation_livre.php");
        echo $e;
    }


    }
}
