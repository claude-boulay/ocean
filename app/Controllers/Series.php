<?php
namespace App\Controllers;

use App\Models\Serie;

class Series extends BaseController
{ 
    public function creationSerie(){

        echo view("templates/header");
        echo view("creation_serie");
    }

    public function validationCreationSerie(){
        if ($this->request->getMethod() === 'post') {
            //on traite les données postées
           
                //la validation a réussi
                $Serie = new Serie();

                $Serie->nom_série= $this->request->getPost('nom_serie');
                $Serie->nbr_de_livre=$this->request->getPost('nbr');
                $Serie->save();
                echo view("templates/header");
                echo view("Validation");
            } else {
                //la validation a échoué
   
                //on transmet les erreurs de validation
                $data['validation'] = $this->validator;
                echo view("templates/header");
                echo view("Echec",$data);
            }
    }
 }

