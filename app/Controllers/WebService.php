<?php
namespace App\Controllers;

use App\Models\Auteur;
use App\Models\Categorie;
use App\Models\Livre;
use App\Models\Serie;

class WebService extends BaseController
{ 
    public function getCategories(){
        $categories=Categorie::all();
        return json_encode($categories,JSON_UNESCAPED_UNICODE);
    }

    public function getLivreByCategorie($idcategorie){
        $categorie=Categorie::find($idcategorie);
        $livres=$categorie->Livres()->get();
        return json_encode($livres,JSON_UNESCAPED_UNICODE);
    }

    public function getSeries(){
        $Séries=Serie::all();
        return json_encode($Séries,JSON_UNESCAPED_UNICODE);
    }

    public function getLivres(){
        $livres=Livre::all();
        return json_encode($livres,JSON_UNESCAPED_UNICODE);
    }

    public function getBookBySeries($idseries){
        $serie=Serie::find($idseries);
        $books=$serie->Livre()->get();
        return json_encode($books,JSON_UNESCAPED_UNICODE);
    }

    public function getSerie($id){
        $serie=Serie::find($id);
        return json_encode($serie,JSON_UNESCAPED_UNICODE);
    }

}