<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function index()
    {
        echo view("templates/header");
        echo view('index');
        
    }
    
    public function inscription(){

        echo view("templates/header");
        echo view("inscription");
    }
}
